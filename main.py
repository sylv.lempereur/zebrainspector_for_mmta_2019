# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This function computes a depth depedant contrast correction,
a segmentation of the whole larvae and a segmentation of the white matter.
"""

# Standard import

from time import time

from os import makedirs
from os.path import exists

# Non stadard imports

from openpyxl import Workbook

# Local imports

from scripts.contrast_correction import depth_contrast_correction

from scripts.metric_computation import get_segmentation_metrics

from scripts.segmentations import white_matter
from scripts.segmentations import whole_larvae

from core.handling import DataHandling
def segmentation(
        path_input: str,
        path_output: str,
        sample: str,
        book: tuple,
        nomenclature: tuple = (
            '@', # Delimitation between channels
            'C01', # Channel idneitifier
            '.mha' # gile extension
            ),
        ):
    """
    Compute segmentation of white matter and whole larve
    of whole mounted 5dpf zebrafish.

    Returns
    -------
    Exit status
    """
    dye = DataHandling(
        path_input\
            + sample\
            + nomenclature[0]\
            + nomenclature[1]\
            + nomenclature[2],
        path_output + '/' + sample + '/',
        printable=True)

    dye.write_step()

    book[0].active["A" +str(book[1])].value = sample

    start_total = time()

    whole_larvae(dye)

    sys.exit()

    time_formated =\
        str(int((time()-start_total)//60))\
        +" min "\
        + str(int((time()-start_total)%60))\
        + 'sec'

    book[0].active["B" +str(book[1])].value = time_formated
    start = time()

    depth_contrast_correction(dye)

    time_formated =\
        str(int((time()-start)//60))\
        +" min "\
        + str(int((time()-start)%60))\
        + 'sec'

    book[0].active["C" +str(book[1])].value = time_formated
    start = time()

    white_matter(dye)

    time_formated =\
        str(int((time()-start)//60))\
        +" min "\
        + str(int((time()-start)%60))\
        + 'sec'

    book[0].active["D" +str(book[1])].value = time_formated

    time_formated =\
        str(int((time()-start_total)//60))\
        +" min "\
        + str(int((time()-start_total)%60))\
        + 'sec'
    book[0].active["E" +str(book[1])].value = time_formated

    return True

def metric(
        list_files: list,
        folder_specs,
        folder_auto: str,
        path_output: str
        ):
    """
    Compute Dice, general balance metrics and Matthew correlation coefficient.

    Parameters
    ----------
    list_files: list
        List of name of samples on which metrics will be computed

    folder_spec_1: str
        Path to the folder that contains manual segmentation
        of the first specialist

    folder_spec_2: str
        Path to the folder that contains manual segmentation
        of the second specialist

    folder_auto: str
        Path to the folder that contains
        automatic segmentation of the whole larvae

    path_output: str
        Path used to save results on a xlsx format.

    Returns
    -------
    exit status
    """

    get_segmentation_metrics(
        list_files=list_files,
        folder_specs=folder_specs,
        folder_auto=folder_auto,
        path_output=path_output
        )

    return True

def main(
        list_files: list,
        path_input: str,
        path_output: str,
        folder_specs: tuple = (None, None),
        nomenclature: tuple = (
            '@', # Delimitation between channels
            'C01', # Channel idneitifier
            '.mha' # gile extension
            )
        ):
    """
    """
    book = Workbook()
    book.active.title = "computation time"
    book.active["A1"].value = "Sample"
    book.active["B1"].value = "Whole larvae"
    book.active["C1"].value = "Contrast correction"
    book.active["D1"].value = "White matter"
    book.active["E1"].value = "Total"
    if path_input[-1] != '/':
        path_input += '/'
    if path_output[-1] != '/':
        path_output += '/'
    if not exists(path_input):
        raise ValueError(path_input + " does not exist.")
    if not exists(path_output):
        print(path_output + " does not exist.\n It will be created.")
        makedirs(path_output)
    pos = 2

    for file in list_files:
        print(file)
        segmentation(
            path_input=path_input,
            path_output=path_output,
            sample=file,
            nomenclature=nomenclature,
            book=(book, pos)
            )
        pos += 1

    book.save(path_output + 'times.xlsx')


    if not None in folder_specs:
        get_segmentation_metrics(
            list_files=list_files,
            folder_specs=folder_specs,
            folder_auto=path_output,
            path_output=path_output + 'metrics.xlsx'
            )

PATHIN = "/data/HcsDatasetsRestitched/Chosen_ones/"

PATHOUT = '/data/hcsProcessing/mmta_2019/v0.0.20.02/'
SAMPLES = [
    # Original datasets
    'HCS--2018_11_28_16_40_21@spec--U00--V00_merge',
    # 'HCS--2018_11_28_16_40_21@spec--U01--V00_merge',
    # 'HCS--2018_11_28_16_40_21@spec--U01--V01_merge',
    # 'HCS--2018_11_28_16_40_21@spec--U02--V02_merge',
    # 'HCS--2018_11_28_16_40_21@spec--U03--V00_merge',
    # 'HCS--2018_11_28_16_40_21@spec--U03--V01_merge',
    # # Datasets for revision
    # # 'HCS--2018_11_30_15_45_33@spec--U00--V00_merge',
    # 'HCS--2018_11_30_15_45_33@spec--U00--V01_merge',
    # 'HCS--2018_11_30_15_45_33@spec--U00--V02_merge',
    # 'HCS--2018_11_30_15_45_33@spec--U02--V00_merge',
    # # 'HCS--2018_11_30_15_45_33@spec--U02--V01_merge',
    # 'HCS--2018_11_30_15_45_33@spec--U02--V02_merge',
    # 'HCS--2018_11_30_15_45_33@spec--U03--V01_merge',
    # 'HCS--2018_11_30_15_45_33@spec--U03--V02_merge'
    ]

FOLDER_SPEC_1 = '/data/data_for_mmta_2019/Segmentations/Manuals/Matthieu/'
FOLDER_SPEC_2 = '/data/data_for_mmta_2019/Segmentations/Manuals/Sylvain/'

# FOLDER_SPEC_1 = None
# FOLDER_SPEC_2 = None

main(
    list_files=SAMPLES,
    path_input=PATHIN,
    path_output=PATHOUT,
    folder_specs=(FOLDER_SPEC_1, FOLDER_SPEC_2),
    nomenclature=(
        '@', # Delimitation between channels
        'C01', # Channel idneitifier
        '.mha' # gile extension
        )
    )
