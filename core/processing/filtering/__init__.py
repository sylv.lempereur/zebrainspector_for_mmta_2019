"""
Filters tools
==========
================================== =============================================
filtering
================================================================================
median                             Median filter computation
================================== =============================================
"""

from .median import median
