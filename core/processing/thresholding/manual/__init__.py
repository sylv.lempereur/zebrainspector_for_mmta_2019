"""
Thresholding process
===========================

================================== =============================================
thresholding
================================================================================
manual                             Thresholding using the given value
manual_percent_histo               Thresholding using the given percent of histo
================================== =============================================
"""

from .manual import manual
from .manual_percent_histo import manual_percent_histo
