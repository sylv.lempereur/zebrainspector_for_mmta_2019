# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This file contains function to label each object in a piece of data.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from scipy.ndimage.measurements import label as scipy_label

from ...handling import DataHandling

def _label_array(
        array: ndarray
        ):
    """
    Labels each object in a boolean image

    Parameters
    ----------
    array: numpy.ndarray
        A binary array

    Returns
    -------
    numpy.ndarray
        Result of the computation of the distance map
    """
    if array.dtype != 'bool':
        raise TypeError('Provided piece of data is not boolean.')
    return scipy_label(array)

def _label_data_handling(
        data_handling_instance: DataHandling
        ):
    """
    Labvel each object of the current step.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    Returns
    -------
    int:
        Number of features
    """
    data_handling_instance.add_step("label")
    array, num_features = _label_array(data_handling_instance.get_current())
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return num_features

def label(data):
    """
    Label each object of the given piece of data.
    If data is a DataHandling instance, the current step will be used.

    Parameters
    ----------
    data:
        could be a numpy.ndarray or a DataHandling instance
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _label_array(data)
    elif isinstance(data,
                    DataHandling
                    ):
        out = _label_data_handling(data)
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
