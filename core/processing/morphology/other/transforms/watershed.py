# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
This modules contains any function that compute a morphological watershed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from skimage.morphology import watershed as watershed_skimage

from .....handling import DataHandling

def _watershed_array(
        array: ndarray,
        markers: ndarray,
        mask: ndarray = None
        ):
    """
    Compute a watershed using given markers on the given array.

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the watershed will be computed
    """
    if mask:
        raise ValueError('masked is not implemented yet')
    array = watershed_skimage(
        array,
        markers
        )
    garbage_collection()
    return array

def _watershed_data_handling(
        data_handling_instance: DataHandling,
        markers: ndarray,
        mask: ndarray = None
        ):
    """
    Compute the  watershed on the current step of a DataHandling instance
    using the markers.

    Parameters
    ----------
    data_handling_instance: DataHandling
        Storage of the current step of the image processing process
    """
    data_handling_instance.add_step("watershed")
    array = data_handling_instance.get_current().copy()
    array = _watershed_array(
        array,
        markers,
        mask
        )
    data_handling_instance.set_current(array)
    garbage_collection()
    return True

def watershed(
        data,
        markers: ndarray,
        mask: ndarray = None
        ):
    """
    Compute a watershed using given markers.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data used to compute the watershed.
        could be a numpy.ndarray or a DataHandling instance
    """
    if not isinstance(markers,
                      ndarray
                      ):
        raise TypeError(
            'Provided markers is not a numpy.ndarray'
            )
    if isinstance(data,
                  ndarray):
        out = _watershed_array(
            data,
            markers,
            mask
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _watershed_data_handling(
            data,
            markers,
            mask
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
