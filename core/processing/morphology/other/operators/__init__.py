"""
Mathematical morphology operators
=================================

================================== =============================================
morphology.operators
================================================================================
closing                            Closing with given radius and shape
dilation                           Dilation with given radius and shape
gradient                           Morphological gradients
erosion                            Erosion with given radius and shape
opening                            Opening with given radius and shape
================================== =============================================
"""
from .closing import \
    closing

from .dilation import \
    dilation

from .erosion import \
    erosion

from .gradient import gradient

from .opening import\
    opening
