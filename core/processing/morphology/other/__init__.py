"""
Mathematical morphology image processing tools without using pink library.
==============================================

================================== =============================================
morphology
================================================================================
closing                            Closing with given radius and shape
dilation                           Dilation with given radius and shape
erosion                            Erosion with given radius and shape
gradient                           Morphological gradient
opening                            Opening with given radius and shape
watershed                          Watershed computation
================================== =============================================
"""

from .operators import\
    closing,\
    dilation,\
    gradient,\
    erosion,\
    opening

from .transforms import watershed
