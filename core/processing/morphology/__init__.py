"""
Mathematical morphology image processing tools
==============================================

================================== =============================================
morphology
================================================================================
closing                            Closing with given radius and shape
dilation                           Dilation with given radius and shape
erosion                            Erosion with given radius and shape
gradient                           Morphological gradient
opening                            Opening with given radius and shape
watershed                          Watershed computation
================================== =============================================
"""

try:

    from .pink import\
        closing,\
        dilation,\
        gradient,\
        erosion,\
        opening,\
        watershed

    print("Pink will be used.")

except ModuleNotFoundError as pink_import_error:
    print(pink_import_error)
    from .other import\
        closing,\
        dilation,\
        gradient,\
        erosion,\
        opening,\
        watershed
