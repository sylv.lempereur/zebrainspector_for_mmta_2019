# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that perform
a erosion.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

# pylint: disable=no-name-in-module
from pink import pink2numpy
from pink.cpp import erosion as erosion_pink
from pink.cpp import geoeros as geoeros_pink
from pink.cpp import feroderect as feroderect_pink

from ..utilities import\
    _numpy2pink_conversion,\
    _generate_structuring_element

from .....handling import DataHandling

def _erosion_array(
        array: ndarray,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    Erosion of a provided array by a given radius
    This script automatically take care of the dimension of the given array

    Parameters
    ----------
    array: numpy.ndarray
        Array used to c ompute a erosion

    rad: int
        Radius of the structuring element

    shape: str
        Shape of the structuring element

    Out
    ---
    numpy.ndarray: result of the erosion

    See also
    --------
    erosion
    opening_array
    """
    if shape == 'rect':
        array = _erosion_rect(
            array,
            rad
            )
    elif shape == 'ball':
        array = _erosion_ball(
            array,
            rad
            )
    garbage_collection()
    return array

def _erosion_ball(
        array: ndarray,
        rad: int = 3
        ):
    """
    Erosion of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Radius of the sphere used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the spherical erosion

    See also
    --------
    opening
    """
    array_pink = _numpy2pink_conversion(array)
    struct = _generate_structuring_element(
        array,
        rad=rad
        )
    array_pink = erosion_pink(
        array_pink,
        struct
        )
    del struct
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def _erosion_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 3,
        shape: str = 'rect'
        ):
    """
    Erosion of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    rad : radius
        Radius of the sphere used as a sructuring element
    See also
    --------
    opening
    """
    data_handling_instance.add_step("erosion" + str(rad))
    array = _erosion_array(
        data_handling_instance.get_current(),
        rad,
        shape
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def _erosion_geodesic_array(
        array: ndarray,
        mask: ndarray
        ):
    """
    Geodesic erosion of the given array using the given mask.
    Both have to be booleans.

    Parameters
    ----------
    array: numpy.ndarray
        Array that will be eroded.
        Have to be a boolean

    mask: numpy.ndarray
        array used as a mask for the geodesic erosion
        Have to be a boolean
    """
    if array.dtype != 'bool':
        raise TypeError("Provided array is not a boolean.")
    array_pink = _numpy2pink_conversion(array)
    array_pink = geoeros_pink(
        array_pink,
        _numpy2pink_conversion(mask),
        connexity=18
        )
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def _erosion_geodesic_data_handling(
        data_handling_instance: DataHandling,
        mask: ndarray
        ):
    """
    Geodesic erosion of the current step using the given mask.
    Both have to be booleans.

    Parameters
    ----------
    array: numpy.ndarray
        An instance of DataHanling class

    mask: numpy.ndarray
        array used to compute the geodesic erosion
        Have to be a boolean
    """
    data_handling_instance.add_step('erosion_geodesic')

    array = _erosion_geodesic_array(
        data_handling_instance.get_current(),
        mask
        )

    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def _erosion_rect(
        array: ndarray,
        rad: int = 3,
        ):
    """
    Erosion of a grey scale image
    using a rectangular structuring element
    of edge size rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Size of edges of the cube used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the cubic erosion

    See also
    --------
    _erosion_ball
    erosion
    """
    array_pink = _numpy2pink_conversion(array)
    array_pink = feroderect_pink(
        array_pink,
        rad,
        rad,
        rad
        )
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def erosion(
        data,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    erosion of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the erosion.
        could be a numpy.ndarray or a DataHandling instance

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    erosion
    """
    if shape not in ['ball', 'rect']:
        raise ValueError("Provided shape is not valid.")
    if isinstance(data,
                  ndarray
                  ):
        out = _erosion_array(
            data,
            rad,
            shape
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _erosion_data_handling(
            data,
            rad,
            shape
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out

def erosion_geodesic(
        data,
        mask: ndarray
        ):
    """
    Geodesic erosion using the given mask. Both have to be booleans.

    Parameters
    ----------
    data:
        Data used a a seed for the geodesic erosion

    mask: numpy.ndarray
        array used to compute the geodesic erosion
        Have to be a boolean

    See also
    --------
    erosion
    closing_geodesic
    dilation_geodesic
    opening_geodesic
    """
    if mask.dtype != 'bool':
        raise TypeError("mask is not a binary image")
    if isinstance(data,
                  ndarray
                  ):
        out = _erosion_geodesic_array(
            data,
            mask
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _erosion_geodesic_data_handling(
            data,
            mask
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
