# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that perform
a dilation.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

# pylint: disable=no-name-in-module
from pink import pink2numpy
from pink.cpp import dilation as dilation_pink
from pink.cpp import geodilat as geodilat_pink
from pink.cpp import fdilaterect as fdilaterect_pink

from ..utilities import\
    _numpy2pink_conversion,\
    _generate_structuring_element

from .....handling import DataHandling

def _dilation_array(
        array: ndarray,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    Dilation of a provided array by a given radius
    This script automatically take care of the dimension of the given array

    Parameters
    ----------
    array: numpy.ndarray
        Array used to c ompute a dilation

    rad: int
        Radius of the structuring element

    shape: str
        Shape of the structuring element

    Out
    ---
    numpy.ndarray: result of the dilation

    See also
    --------
    dilation
    opening_array
    """
    if shape == 'rect':
        array = _dilation_rect(
            array,
            rad
            )
    elif shape == 'ball':
        array = _dilation_ball(
            array,
            rad
            )
    garbage_collection()
    return array

def _dilation_ball(
        array: ndarray,
        rad: int = 3
        ):
    """
    Dilation of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Radius of the sphere used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the spherical dilation

    See also
    --------
    opening
    """
    array_pink = _numpy2pink_conversion(array)
    struct = _generate_structuring_element(
        array,
        rad=rad
        )
    array_pink = dilation_pink(
        array_pink,
        struct
        )
    del struct
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def _dilation_data_handling(
        data_handling_instance: DataHandling,
        rad: int = 3,
        shape: str = 'rect'
        ):
    """
    Dilation of a grey scale image
    using a spherical structuring element
    of radius rad.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    rad : radius
        Radius of the sphere used as a sructuring element
    See also
    --------
    opening
    """
    if shape not in ['ball', 'rect']:
        raise ValueError("Provided shape is not valid.")
    data_handling_instance.add_step("dilation" + str(rad))
    array = _dilation_array(
        data_handling_instance.get_current(),
        rad,
        shape
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def _dilation_geodesic_array(
        array: ndarray,
        mask: ndarray
        ):
    """
    Geodesic dilation of an array using the given mask.
    Both have to be booleans.

    Parameters
    ----------
    array: numpy.ndarray
        A boolean array that will be used as a seed for the geodesic dilation.

    mask: numpy.ndarray
        array used to compute the geodesic dilation
        Have to be a boolean

    See also
    --------
    erosion_geodesic
    dilation
    """
    if array.dtype != 'bool':
        raise TypeError("Current step is not a binary image")
    array_pink = _numpy2pink_conversion(array)
    mask_pink = _numpy2pink_conversion(mask)
    array_pink = geodilat_pink(
        array_pink,
        mask_pink,
        connexity=18
        )
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def _dilation_geodesic_data_handling(
        data_handling_instance: DataHandling,
        mask: ndarray
        ):
    """
    Geodesic dilation of an array using the given mask.
    Both have to be booleans.

    Parameters
    ----------
    data_handling_instance: DataHandling
        An instance of DataHanling class

    mask: numpy.ndarray
        array used to compute the geodesic dilation
        Have to be a boolean

    See also
    --------
    erosion_geodesic
    dilation
    """
    data_handling_instance.add_step('dilation_geodesic')
    array = _dilation_geodesic_array(
        data_handling_instance.get_current(),
        mask
        )
    data_handling_instance.set_current(array)
    del array
    garbage_collection()
    return True

def _dilation_rect(
        array: ndarray,
        rad: int = 3,
        ):
    """
    Dilation of a grey scale image
    using a cubic structuring element
    of edge size rad.

    Parameters
    ----------
    array: numpy.ndarray
        array to close

    rad : radius
        Size of edges of the cube used as a sructuring element

    Out
    ---
    numpy.ndarray: result of the cubic dilation

    See also
    --------
    _dilation_ball
    dilation
    """
    array_pink = _numpy2pink_conversion(array)
    array_pink = fdilaterect_pink(
        array_pink,
        rad,
        rad,
        rad
        )
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def dilation(
        data,
        rad: int = 3,
        shape: str = 'ball'
        ):
    """
    dilation of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the dilation.
        could be a numpy.ndarray or a DataHandling instance

    rad : radius
        Radius of the sphere used as a sructuring element

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    dilation
    """
    if isinstance(data,
                  ndarray
                  ):
        out = _dilation_array(
            data,
            rad,
            shape
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _dilation_data_handling(
            data,
            rad,
            shape
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out

def dilation_geodesic(
        data,
        mask: ndarray
        ):
    """
    dilation of an image
    using a structuring element of a given shape
    with a radius rad.

    Parameters
    ----------
    data:
        Data use to perform the dilation.
        could be a numpy.ndarray or a DataHandling instance

    mask: numpy.ndarray
        Array used as a mask

    shape: str
        Shape of the structuring element. Could be ball or rect

    See also
    --------
    dilation
    """
    if mask.dtype != 'bool':
        raise TypeError('Invalid type for the mask image')
    if isinstance(data,
                  ndarray
                  ):
        out = _dilation_geodesic_array(
            data,
            mask
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _dilation_geodesic_data_handling(
            data,
            mask
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
