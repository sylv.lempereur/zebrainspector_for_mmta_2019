# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that compute a morphological watershed.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection

from numpy import ndarray

from skimage.morphology import watershed as watershed_skimage

# pylint: disable=no-name-in-module
from pink import pink2numpy
from pink.cpp import\
    watershedMeyer2labnomask,\
    watershedMeyer2NM

from ..utilities import _numpy2pink_conversion

from .....handling import DataHandling

def _watershed_array(
        array: ndarray,
        markers: ndarray,
        mask: ndarray = None
        ):
    """
    Compute a watershed using given markers on the given array.

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the watershed will be computed
    """
    if mask:
        raise ValueError('masked is not implemented yet')
    if array.max() > 255:
        array = _watershed_not_byte(
            array,
            markers
            )
    else:
        if markers.max() > 2:
            array = _watershed_lab_nomask(
                array,
                markers
                )
        else:
            array = _watershed_nolab_nomask(
                array,
                markers
                )
    garbage_collection()
    return array

def _watershed_data_handling(
        data_handling_instance: DataHandling,
        markers: ndarray,
        mask: ndarray = None
        ):
    """
    Compute the  watershed on the current step of a DataHandling instance
    using the markers.

    Parameters
    ----------
    data_handling_instance: DataHandling
        Storage of the current step of the image processing process
    """
    data_handling_instance.add_step("watershed")
    array = data_handling_instance.get_current().copy()
    array = _watershed_array(
        array,
        markers,
        mask
        )
    data_handling_instance.set_current(array)
    garbage_collection()
    return True

def _watershed_lab_nomask(
        array: ndarray,
        markers: ndarray
        ):
    """
    Compute a watershed using given markers on the given array.

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the watershed will be computed
    """
    array_pink = _numpy2pink_conversion(array.astype('uint8'))
    markers_pink = _numpy2pink_conversion(markers.astype('int32'))
    array_pink = watershedMeyer2labnomask(
        array_pink,
        markers_pink,
        18
        )
    del markers_pink
    array = pink2numpy(array_pink).copy()
    del array_pink
    garbage_collection()
    return array

def _watershed_nolab_nomask(
        array: ndarray,
        markers: ndarray
        ):
    """
    Compute a watershed using given markers on the given array.

    Parameters
    ----------
    array: numpy.ndarray
        Array on wich the watershed will be computed
    """
    array_pink = _numpy2pink_conversion(array.astype('uint8'))
    markers_pink = _numpy2pink_conversion(markers.astype('int32'))
    print(array_pink)
    print(markers_pink)
    array_pink = watershedMeyer2NM(
        array_pink,
        markers_pink,
        18
        )
    array = pink2numpy(array_pink).copy()
    garbage_collection()
    return array

def _watershed_not_byte(
        array: ndarray,
        markers: ndarray
        ):
    """"
    Computation of a watershed using given array
    and the given markers

    Parameters
    ----------
    array: numpy.ndarray
        Array used to compute the watershed

    markers: ndarray
        array that give values to assig during our watershed
    """
    array = watershed_skimage(
        array,
        markers
        )
    return array

def watershed(
        data,
        markers: ndarray,
        mask: ndarray = None
        ):
    """
    Compute a watershed using given markers.
    DataHandling instance or numpy array could be provided.

    Parameters
    ----------
    data:
        Data used to compute the watershed.
        could be a numpy.ndarray or a DataHandling instance
    """
    if not isinstance(markers,
                      ndarray
                      ):
        raise TypeError(
            'Provided markers is not a numpy.ndarray'
            )
    if isinstance(data,
                  ndarray):
        out = _watershed_array(
            data,
            markers,
            mask
            )
    elif isinstance(data,
                    DataHandling
                    ):
        out = _watershed_data_handling(
            data,
            markers,
            mask
            )
    else:
        raise TypeError(
            'Provided data is not a numpy.ndarray nor a DataHandling instance'
            )
    garbage_collection()
    return out
