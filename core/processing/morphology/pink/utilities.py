# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
# Due to a problem with pink, have to disable no-member
# pylint: disable=no-member
"""
This modules contains any function that perform
a mathematical morphology computation.

This files is used when pink is installed.
"""
from gc import collect as garbage_collection
from numpy import ndarray

from pink import numpy2pink
# pylint: disable=no-name-in-module
from pink.cpp import genball

def _generate_structuring_element(
        array: ndarray,
        rad: int
        ):
    """
    Generate structuring element.

    Parameters
    ----------
    array: numpy.ndarray
        Array use to found structuring element dimension.

    rad : radius
        Radius of the sphere used as a sructuring element

    returns
    -------
    pink.char_image
        Structuring element with the wanted radius
    """
    dimension = len(array.shape)
    struct = genball(
        rad,
        dimension=dimension
        )
    del dimension
    garbage_collection()
    return struct

def _numpy2pink_conversion(
        array: ndarray
        ):
    """
    This function change uint16 into uint32 and bool into uint8
    for compatibility.

    Pink only accept uint8, uint16, float32 or float 64 array.

    Parameters
    ----------
    array: numpy.ndarray

    returns
    -------
    pink_array
        Pink array with the workable data type
    """
    if array.dtype in ['int16', 'int32', 'uint16', 'uint32', 'float32']:
        array = array.astype('int32')
    elif array.dtype in ['bool', 'uint8', 'int8']:
        array = array.astype('uint8')
    else:
        raise TypeError('Array dtype not usable yet.')
    array_pink = numpy2pink(array)
    del array
    garbage_collection()
    return array_pink
