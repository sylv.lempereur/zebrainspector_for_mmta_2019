"""
Basics processing
==================================

================================== =============================================
basics
================================================================================
frame_creation                     Creation of a frame array
intersection_removal               Remove intersection between two piece of data
================================== =============================================
"""

from .frame_creation import frame_creation
from .intersection_removal import intersection_removal
