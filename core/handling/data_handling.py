## @package Classes.imageProcessing
# Class that perform the image processing
# @file
# This file contains the class that perform the image processing
"""
Classes and functions used to handle with data
"""

from os import makedirs

from os.path import\
    exists,\
    realpath,\
    split,\
    splitext

from numpy import ndarray

from SimpleITK import\
    GetImageFromArray,\
    GetArrayFromImage,\
    ReadImage,\
    WriteImage

class DataHandling:
    """
    This Class is used to store images to reduce the usage of ram

    Parameters
    ----------
    file_input: str
        Path to the original image

    path_output: str
        Results storage location

    printable: bool
        Allow to displays steps

    Attributes
    ----------
    _internal_attributes :tuple
        Lists the location used to store computation results,
        the name of the image and the printable status.

    _step: list
        list of computed step

    _spacing: tupple
        Physical dimensions of a voxel

    _original: numpy.ndarray
        Array corresponding to the input image

    _current: numpy.ndarray
        Current stade of the image

    _larvae: numpy.ndarray
        Segmentation of the whole larvae

    _mask: numpy.ndarray
        Array used as a mask for processing
    """
    def __init__(self,
                 file_input: str,
                 path_output: str,
                 printable: bool = False
                ):
        self._step = []

        if not exists(file_input):
            raise ValueError("Your input file does not exist")

        _, name = split(file_input)

        name, extension = splitext(name)
        if extension != ".mha":
            raise ValueError("Your input file is not a mha file")

        if path_output[-1] != "/":
            path_output = path_output + "/"
        if exists(realpath(path_output)) is False:
            makedirs(path_output)

        self._internal_attributes = (
            path_output,
            name,
            printable
            )
        temp = ReadImage(file_input)
        self._spacing = temp.GetSpacing()

        self._original = GetArrayFromImage(temp)
        if self._original.dtype != "uint16":
            self._original *= float(2 ** 12) / float(self._original.max())
            self._original = self._original.astype("uint16")

        self._current = self._original.copy()
        self._larvae = None
        self._mask = None

    def _type_verification(self):
        """
        TODO: write this documentation and deals with more kinf of data
        """
        # array = self.get_current().copy()
        # if issubclass(
        #         array.dtype.type,
        #         integer
        #     ):
        if self._current.max() < 2:
            self._current = self._current.astype("bool")
        elif self._current.max() < 2 ** 8:
            self._current = self._current.astype("uint8")
        elif self._current.max() < 2** 16:
            self._current = self._current.astype("uint16")
        else:
            self._current = self._current.astype('int32')
        return True

    def add_step(self,
                 step_name: str
                 ):
        """
        Add the name of the current step in a list.
        from this list, the number of the performed step could be found.

        If print are allowed, the given name will be print in terminal.

        Parameters
        ----------
        step_name: str
            Name of the step that will be performed
        """
        self._step.append(step_name)
        if self._internal_attributes[2]:
            print("\t" + step_name)
        return True

    def get_current(self):
        """
        return current status of the image
        """
        return self._current


    def get_larvae(self):
        """
        Return larvae segmentation
        """
        return self._larvae

    def get_mask(self):
        """
        Return mask
        """
        return self._mask

    def get_original(self):
        """
        Return original image
        """
        return self._original

    def get_spacing(self):
        """
        Return spacing of the image
        """
        return self._spacing

    def get_step(self):
        """
        Return name of the current step
        """
        return self._step[len(self._step) - 1]

    def reboot(self):
        """
        Reboot every element of the class to the initial stage
        """
        # Reset the image.
        del self._current, self._larvae, self._mask
        self._current = self._original.copy()
        self._larvae = None
        self._mask = None
        # Empty the step list.
        self._step = []
        return True

    def set_current(self,
                    array: ndarray,
                    tag: str = None
                    ):
        """
        Store an array as the current step

        Parameters
        ----------
        array: numpy.ndarray
            Array to store ass the current step
        """
        if tag:
            self._step.append(tag)
        del self._current
        self._current = array
        self._type_verification()
        return True

    def set_larvae(self,
                   array: ndarray = None
                   ):
        """
        Store a whole larvae segmentation

        If no array is given,
        the current stage will be consider as the segmentation

        Parameters
        ----------
        mask: numpy.ndarray
            (optionnal) array to store as a segmentation of the whole larvae

        """
        if array is not None:
            del self._larvae
            self._larvae = array
        else:
            del self._larvae
            self._larvae = self._current
        return True

    def set_mask(self,
                 mask: ndarray = None
                 ):
        """
        Store an array as a mask.

        If no array is provided, the current step will be stored as a mask.

        Parameters
        ----------
        mask: numpy.ndarray
            (optionnal) array to store as a mask
        """
        # If no image was given
        if mask is None:
            # Fulfill the mask variable with the current step
            del self._mask
            self._mask = self._current
        else:
            del self._mask
            # Set the mask image with the given array.
            self._mask = mask
        return True

    def set_original(self,
                     array: ndarray
                     ):
        """
        Change the image consider as original

        Parameters
        ----------
        array: numpy.ndarray
            array to store as the original image
        """
        self._original = array
        return True

    def get_path(self):
        """
        Return path and name of the iamge

        Parameters
        ----------
        path: str
            Path and name of the image
        """
        return self._internal_attributes[0] + self._internal_attributes[1]

    def write_step(self,
                   tag: str = None
                   ):
        """
        Write a mha file with the current step of the process.
        The file name will be store like imageName_stepNumber_stepName

        However, if you provide a specific tag,
        file name will be imageName_tag

        Parameters
        ----------
        tag: str
            optionnal : choose a specific tag
        """
        # If no tag is provided
        if tag is None:
            # If at least one step was computed
            if self._step:
                # Creation of the save path that is formed with
                # the output folder setup during the initialisation,
                # the name of the image,
                # the step number,
                # and the descriptor of the current step.
                path_saving =\
                    self._internal_attributes[0]\
                    + self._internal_attributes[1]\
                    + "_step"\
                    + str(len(self._step))\
                    + "_"\
                    + self._step[len(self._step) - 1]\
                    + '.mha'
            # If no process was performed
            else:
                # The name is just formed with the ouptu path
                # and the name of the iamge.
                path_saving =\
                    self._internal_attributes[0]\
                    + self._internal_attributes[1]\
                    +'_original'\
                    + '.mha'
        # if a tog was given,
        else:
            # The saving path is formed using the ouput folder,
            # The name of the image and the tag.
            path_saving = \
                self._internal_attributes[0]\
                + self._internal_attributes[1]\
                + "_"\
                + tag\
                + '.mha'
        # The step is converted from an array to an itk image.
        # However, simpleITK can't save boolean image
        # This images are convert to uint8
        if self._current.dtype == 'bool':
            to_saved = GetImageFromArray(self._current.astype('uint8'))
        else:
            to_saved = GetImageFromArray(self._current)

        # The spacing of the original image is set to the image to save.
        to_saved.SetSpacing(self._spacing)
        # The file is written.
        WriteImage(to_saved, path_saving)
        return True
