# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform segmentation-based depth-dependant contract correction
"""

from core.handling import DataHandling

from core import processing

def depth_contrast_correction(
        dye_storage: DataHandling
        ):
    """
    Compute contrast correction of huC and diI channelrs.

    Parameters
    ----------
    dye_storage: DataHandling
        Storage of the DiI channel

    huc_storage: DataHandling
        Storage of the HuC channel
    """
    print('Image improvement')
    ##Detection of the depth of each pixel
    if dye_storage.get_mask() is not None:
        dye_storage.set_current(dye_storage.get_mask())
    elif dye_storage.get_current().dtype != 'bool':
        raise TypeError(
            "At this step, a mboolean mask should be stored"\
            +" or the current step should be a boolean array"
            )

    processing.labeling.depth(dye_storage)
    dye_storage.write_step()

    # Set his mask to the Hu
    # huc_storage.set_current(dye_storage.get_current())

    processing.contrast_correction.segmentation_based_depth_dependant(
        dye_storage
        )

    dye_storage.write_step('contrastCorrected')

    # processing.contrast_correction.segmentation_based_depth_dependant(
    #     huc_storage
    #     )

    # huc_storage.write_step('contrastCorrected')

    return True
