# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatic segmentations of whole mounted 5dpf zebrafish
"""

from gc import collect as garbage_collection

from core.handling import DataHandling

from core import processing

def whole_larvae(
        dye_storage: DataHandling
        ):
    """
    Automatic segmentation of the whole larvae using a watershed procedure

    Parameters
    ----------
    dye_storage: DataHandling
        Store the DiI channel

    See also
    --------
    myelin
    """

    print('Whole larvae segmentation')

    dye_storage.get_current()

    processing.morphology.opening(
        data=dye_storage,
        rad=15,
        shape='rect'
        )

    dye_storage.write_step("eq1")

    opened = dye_storage.get_current().copy()

    coordinates = processing.detection.maximum_local(
        dye_storage.get_original()
        )

    dye_storage.set_current(opened.copy())

    processing.thresholding.manual_percent_histo(
        dye_storage,
        1
        )

    dye_storage.write_step("eq2")

    # dye_storage.write_step()

    # # Set the result into the zones mask
    zones = 2 * (dye_storage.get_current().copy() == 1).astype('uint8')

    dye_storage.set_current(zones)
    dye_storage.write_step('zones')

    markers = processing.basics.frame_creation(dye_storage).astype('uint8')
    markers[coordinates] = zones[coordinates]

    dye_storage.set_current(markers)
    dye_storage.write_step('eq4')

    # Back to the opened image
    dye_storage.set_current(opened)

    # Morphological gradient
    processing.morphology.gradient(dye_storage)

    dye_storage.write_step("eq6")

    # dye_storage.write_step()

    # Watershed
    processing.morphology.watershed(
        dye_storage,
        markers
        )

    # dye_storage.write_step()

    processing.thresholding.manual(
        dye_storage,
        2
        )

    dye_storage.write_step("eq7")

    # dye_storage.write_step()

    processing.morphology.closing(
        dye_storage,
        11,
        shape="rect"
        )

    dye_storage.write_step("eq8")

    # dye_storage.write_step()

    processing.labeling.size(dye_storage)

    processing.thresholding.manual(
        dye_storage,
        dye_storage.get_current().max()
        )

    dye_storage.write_step("eq9")

    sys.exit()

    # dye_storage.write_step()

    dye_storage.write_step('whole_larvae_per_watershed')
    # Write the result

    # Store this result as mask and whole larvae segmentation for both channel.
    dye_storage.set_mask()
    dye_storage.set_larvae()

    dye_storage.set_current(dye_storage.get_original().copy())

    garbage_collection()
    return True
