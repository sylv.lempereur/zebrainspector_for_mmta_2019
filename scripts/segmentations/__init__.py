"""
Segmentations scripts
===========================

================================== =============================================
segmentations
================================================================================
white_matter                       Segment white matter of 5dpf larvae
whole_larvae                       Segment whole larvae using watershed
================================== =============================================
"""

from .white_matter import white_matter
from .whole_larvae import whole_larvae
