# author:
#     Sylvain Lempereur <sylv.lempereur@gmail.com>
#
# not licensed yet
"""
Perform automatic segmentations of white matter of a 5dpf zebrafish
"""

from gc import collect as garbage_collection

import numpy

from core.handling import DataHandling

from core import processing

from .eyes import eyes as segmentation_eyes

def white_matter(
        dye_storage: DataHandling
        ):
    """
    Segmentation of the myelin in the brain using DiI channel.

    Parameters
    ----------
    dye_storage: DataHandling
        Store the DiI channel

    See also
    --------
    whole_larvae_per_watershed
    """
    print("Segmentation of brain's myelin")
    # Median filter
    initial = dye_storage.get_current().copy()

    processing.filtering.median(
        dye_storage,
        3
        )

    dye_storage.write_step()

    # Store result of the median filter
    medianed = dye_storage.get_current().copy()

    dye_storage.write_step("eq13")

    # dye_storage.set_current(initial)

    # Otsu thresholding
    processing.thresholding.otsu(
        dye_storage,
        4
        )

    # Work on highest class
    processing.thresholding.manual(
        dye_storage,
        3
        )

    dye_storage.write_step("eq14")

    # Store result
    eyes = segmentation_eyes(
        initial,
        dye_storage.get_path(),
        dye_storage.get_spacing()
        )

    test = dye_storage.get_current()
    test[eyes != 0] = 0

    dye_storage.set_current(eyes)

    dye_storage.write_step('eyes')

    dye_storage.set_current(test)

    dye_storage.write_step("eq20")

    processing.morphology.dilation(
        dye_storage,
        7,
        'rect'
        )

    dye_storage.write_step("eq21")

    up_dilat = dye_storage.get_current().copy()

    processing.morphology.erosion(
        dye_storage,
        7,
        'rect'
        )

    dye_storage.write_step("eq22")

    # Work on the lowest class

    processing.labeling.size(dye_storage)

    processing.thresholding.manual(
        dye_storage,
        dye_storage.get_current().max()
        )

    dye_storage.write_step("eq23")


    zones = 2 * dye_storage.get_current().copy().astype('uint8')

    array_temp = dye_storage.get_mask().copy()
    array_temp[up_dilat != 0] = 0

    del up_dilat

    # Work on the lowest class
    dye_storage.set_current(array_temp.copy())

    dye_storage.write_step("eq24")

    processing.morphology.erosion(
        dye_storage,
        7,
        'rect'
        )

    dye_storage.write_step("eq25")

    zones[dye_storage.get_current() != 0] = 1

    del array_temp

    dye_storage.set_current(zones)
    dye_storage.write_step("eq26")

    #Found local maxima
    coordinates = processing.detection.maximum_local(
        medianed
        )

    # Set local maximum to their "zone" value.
    markers = numpy.zeros(zones.shape)
    markers[coordinates] = zones[coordinates]

    dye_storage.set_current(markers)
    dye_storage.write_step("eq27")

    # Back to the medianed filtered image
    dye_storage.set_current(medianed.copy())

    # Computation of a morphological gradient
    processing.morphology.gradient(dye_storage)
    dye_storage.write_step("eq28")

    #Watersehd
    processing.morphology.watershed(
        dye_storage,
        markers
        )
    dye_storage.write_step("eq29")

    dye_storage.write_step()

    processing.thresholding.manual(
        dye_storage,
        2
        )

    dye_storage.write_step()

    processing.labeling.size(dye_storage)

    processing.thresholding.manual(
        dye_storage,
        dye_storage.get_current().max())

    dye_storage.write_step("eq30")

    dye_storage.write_step()

    dye_storage.write_step("white_matter")

    garbage_collection()

    return True
